<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('pagamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_transacao');
            $table->string('referencia')->nullable();
            $table->string('forma_pagamento');
            $table->timestamp('data_transacao')->nullable();
            $table->decimal('valor', 6, 2)->default(0);
            $table->integer('status');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamento');
    }
}
