<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 28 Apr 2017 02:02:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Produto
 * 
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property float $valor
 * @property string $imagem
 * @property string $ativo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pedidos
 *
 * @package App\Models
 */
class Produto extends Eloquent
{
	protected $casts = [
		'valor' => 'float'
	];

	protected $fillable = [
		'nome',
		'descricao',
		'valor',
		'imagem',
		'ativo'
	];

	public function pedidos()
	{
		return $this->belongsToMany(\App\Models\Pedido::class, 'pedido_produtos')
					->withPivot('id', 'status', 'valor', 'desconto', 'cupom_desconto_id')
					->withTimestamps();
	}
}
