<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 28 Apr 2017 02:02:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Pagamento
 * 
 * @property int $id
 * @property string $id_transacao
 * @property string $referencia
 * @property string $forma_pagamento
 * @property \Carbon\Carbon $data_transacao
 * @property float $valor
 * @property int $status
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Pagamento extends Eloquent
{
	protected $table = 'pagamento';
	public $timestamps = false;

	protected $casts = [
		'valor' => 'float',
		'status' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'data_transacao'
	];

	protected $fillable = [
		'id_transacao',
		'referencia',
		'forma_pagamento',
		'data_transacao',
		'valor',
		'status',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
