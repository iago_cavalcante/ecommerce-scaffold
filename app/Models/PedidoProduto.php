<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 28 Apr 2017 02:02:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PedidoProduto
 * 
 * @property int $id
 * @property int $pedido_id
 * @property int $produto_id
 * @property string $status
 * @property float $valor
 * @property float $desconto
 * @property int $cupom_desconto_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\CupomDesconto $cupom_desconto
 * @property \App\Models\Pedido $pedido
 * @property \App\Models\Produto $produto
 *
 * @package App\Models
 */
class PedidoProduto extends Eloquent
{
	protected $casts = [
		'pedido_id' => 'int',
		'produto_id' => 'int',
		'valor' => 'float',
		'desconto' => 'float',
		'cupom_desconto_id' => 'int'
	];

	protected $fillable = [
		'pedido_id',
		'produto_id',
		'status',
		'valor',
		'desconto',
		'cupom_desconto_id'
	];

	public function cupom_desconto()
	{
		return $this->belongsTo(\App\Models\CupomDesconto::class);
	}

	public function pedido()
	{
		return $this->belongsTo(\App\Models\Pedido::class);
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class);
	}
}
